package polymorphism;

public class ElectronicBook extends Book{
     private int numberBytes;
     
      public ElectronicBook(String title, String author, int numberBytes){
            super(title, author);
            this.numberBytes = numberBytes;
      }

     public int getNumberBytes() {
           return numberBytes;
     }

     public String toString(){
            return super.toString() + " number bytes: " + this.numberBytes;
     }
}
