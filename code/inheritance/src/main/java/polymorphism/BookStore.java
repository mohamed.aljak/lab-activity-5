package polymorphism;

public class BookStore {
      public static void main(String[] args){
            Book[] books = new Book[]{
                  new Book("book a", "author a"),
                  new ElectronicBook("book a", "author a", 60),
                  new Book("book a", "author a"),
                  new ElectronicBook("book a", "author a", 40),
                  new ElectronicBook("book a", "author a", 20)
            };

            for(Book book: books){
                  System.out.println(book);
            }

            ElectronicBook b = (ElectronicBook) books[0];
            System.out.println(b.getNumberBytes());
      }
}
